import Vue from 'vue'
import App from './App.vue'
import VueLogger from './plugins/vueLogger/vueLogger'
import deployMessege from 'plugin-demo-prueba/src/main'

Vue.use(deployMessege);
Vue.use(VueLogger);

new Vue({
  el: '#app',
  render: h => h(App),
  created: () => {
    console.info('text')
    console.error('text')
    console.log('text')
    console.warn('text')
  }
})
