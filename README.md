# PLUG-IN DEMO

Este demo es una prueba rápida de una instalación de una librería propia. La librería comprende la elaboración del componente deployMessege (`<deploy-messege></deploy-messege>`), el cual se encargara de mostrar un mensaje con título y cuerpo y a demás posee dos campos de entradas para cambiar el título del mensaje y el cuerpo del mismo.

***
## Introducción

### Plug-in

Vue permite la creación de librerías propias que desempeñen funcionalidad especifica y que ayudan a complementar todas las funcionalidades que por defecto se encuentran en Vue.

En Vue una librería puede ser definida como uno o un conjunto de elementos, funciones o componentes que posean un método/función **install**. Los plug-ins pueden ser creados por las siguientes razones:

1. Añadir métodos globales o propiedades: para este caso dentro del método **install** debe ser introducido el método global o propiedad de manera global por ejemplo:
```
export default {
    install (Vue,option) {
        Vue.miFuncion = function () {
            /*lógica*/
        }
    }
}
```

2. Añadir directivas, filtros o transiciones globales: si el proyecto lo demanda, los plug-in's pueden ser utilizados para establecer directivas o filtros de manera general. La estructura para que un plug-in pueda establecer una directiva es la siguiente:
```
export default {
    install (Vue,option) {
        Vue.directive('mi-directiva', {
            bind(el,binding, vnode, oldVnode) {
                /*lógica*/
            }
        }
    }
}
```

3. Añadir nuevas funciones de a un componente: mediante el uso de Mixins y con la asociación de plug-in's se pueden establecer funciones de manera general entre componentes e importarlas como nuevas librerías:
```
export default {
    install (Vue,option){
        Vue.mixin ({
            created: function () {
                /*lógica*/
                }
            })
        }
    }
```

4. Añadir métodos a instancias:  se pueden añadir funciones adicionales como librería de complementos a las funciones que ofrece Vue:
```
export default {
    install (Vue,option){
        Vue.prototype.$miMetodo = function () {
            /*lógica*/
        }
    }
}
```

5. Establecer una librería de componentes funcionales: se pueden añadir componentes completos con librerías adicionales para el proyecto:
```
export default {
    install (Vue,option) {
        Vue.component(miComponente.name, miComponente)
    }
}
```

### Uso del Plug-in

Los plug-in al igual que cualquier otra librería que se importe en Vue tienen su procedimiento para poder ser utilizados. Una vez instalado por NPM o importado por CDN al proyecto, este debe ser importado al archivo **principal** del proyecto y decirle a Vue que utilice dicha librería, por ejemplo:
```
import miPlugin from 'direccionDelPlugin/o/MainJsDelPluginEnElProyecto'

Vue.use (miPlugin);
```
Y con eso ya el plug-in puede ser usado en el proyecto.

***
## Construcción del Demo

En la construcción de este demo, se estableció un componente sencillo en Vue para mostrar mostrar un mensaje en la pantalla con mensaje y cuerpo, adicional a esto dos campos de entradas para cambiar el titulo del mensaje y el cuerpo del mismo. El componente diseñado se establece con el tag `<deployMessege>`

Durante la construcción del demo se resaltaron los siguientes eventos:

1. En la construcción del componente: la propiedad **el** no se utiliza  menos que se cree la instancia de Vue, de lo contrario se utiliza la propiedad **name** en su lugar.

2. Si la librería a diseñar posee mas de un componente que pueda ser utilizado de manera individual, se sugiere crear un directorio por cada componente de este tipo (o que comparta funcionalidad) el cual contendrá el componente Vue y un **index.js** que exporte el modelo Vue del componente. El archivo principal del plugin debe realizar la importación de los index.js requeridos para el plugin y establecer el método de instalación para los mismos. (Para mayor referencia, visualizar el repositorio de Buefy)

3. Es posible que en algunos casos la importación del plugin de algún error de reconocimiento (se presento en este plugin), si esto sucede hay que asegurarse de importar el archivo **principal** del plugin instalado. Ver archivo **main.js** del demo.

4. Si se utilizan librerías instaladas por NPM, el package.json actualiza las dependencias de la aplicación, si se utiliza por CDN, hay que tener presente los casos de dependencias y las versiones utilizadas.

5. Para instalar una librería de manera local primero se empaqueta la librería con el comando:
```
npm pack
```
    El cual creara un paquete .tgz en la carpeta del proyecto que se empaqueto. Para instalarlo en otro proyecto se utiliza el comando:
```
npm install ./Direccion/Del/Paquete.tgz
```

6. Si la instalación es por repositorio publico se utiliza:
```
npm install <protocolo>://dominio/username/repositorio<.git><#rama>
```
7. Si la instalación es por repositorio privado se utiliza:
```
npm install <protocolo>://oauth2:<token>@dominio/username/repositorio<.git><#rama>
```
El token debe ser generado por la herramienta utilizada para la gestión del repositorio, para este Demo se utilizo GitLab y para la generación del token se deben realizar los siguientes pasos:
  * Dirigirse al perfil del usuario quien posee el proyecto.
  * Seleccionar la opción **Access Tokens**.
  * Definir en el formulario, definir nombre del token.
  * Definir fecha de expiración (opcional).
  * Definir el alcance del token a generar.
  * Presionar el el botón **Create Personal Access Token**.

8. Las librerías creadas deben tener su package.json en el cual se establezcan las dependencias necesarias para su utilización.
